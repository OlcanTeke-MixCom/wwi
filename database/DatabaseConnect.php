<?php

class DatabaseConnect
{
    private $user = 'root';
    private $password = 'root';
    private $name = 'wwi';
    private $host = 'localhost';

    public function connect(): PDO
    {
        return new PDO('mysql:host='.$this->host.';dbname='.$this->name, $this->user, $this->password, array(
            PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION,
            PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
            PDO::ATTR_EMULATE_PREPARES   => false,
        ));
    }
}
