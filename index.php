<?php

session_start();


include_once 'database/DatabaseConnect.php';
$connect = new DatabaseConnect();
$db = $connect->connect();
$errors = [];
$update = null;

$stmt = $db->prepare('Select naam from categorie');
$stmt->execute();
?>

<html>
<head>
    <link rel="stylesheet" href="css/menu.css">
    <link rel="stylesheet" href="css/grid.css">
    <link rel="stylesheet" href="css/category.css">
    <link rel="stylesheet" href="css/product-details.css">
    <link rel="stylesheet" href="css/cart.css">
    <link rel="stylesheet" href="css/search.css">
</head>
<body>
    <div class="container">
        <div class="Header" >
            <div class="topnav">
                <?php
                if($_GET['page'] =='home') {
                    $home="active";
                }else{
                    $home="";
                }
                if ($_GET['page']=='contact'){
                    $contact="active";
                }else{
                    $contact="";
                }
                if ($_GET['page']=='over'){
                    $over="active";
                }else {
                    $over="";
                }
                if($_GET['page']=='login') {
                    $login = 'active';
                }else {
                    $login = '';
                }
                if($_GET['page']=='registration') {
                    $registration = 'active';
                }else {
                    $registration = '';
                }

                ?>
                <a class="<?= "$home"?>" href=?page=home>Home</a>
                <?php
                    while ($menurow = $stmt->fetch()) {
                        foreach ($menurow as $menuitem) {
                            if ($_GET['category'] == $menuitem) {
                                ${$menuitem} = "active";
                            } else ${$menuitem} = "";
                            echo "<a class='${$menuitem}' href=?page=category&category=" . $menuitem . ">" . $menuitem . "</a>";
                        }
                    }
                ?>
                <a class="<?= "$contact"?>" href="?page=contact">Contact</a>
                <a class="<?= "$over"?>" href="?page=over">Over WWI</a>
                <?php
                if (isset($_SESSION['id'])) {
                    echo '<a href="./?page=login&logout=true">Uitloggen</a>';
                } else {
                    echo "<a class='$login' href='./?page=login'>Inloggen</a>";
                    echo "<a class='$registration' href='./?page=registration'>Registreren</a>";
                }
                ?>
                <a href="./?page=winkelmand">Winkemand: <?= count($_SESSION['cart']) ?></a>

                <form method="post" action="">
                    <input style="width: 100px" type="text" name="searchValue" placeholder="Zoeken">
                    <input type="submit" name="search" value="zoek">
                </form>
            </div>
        </div>
        <div class="Main">
            <?php
            if (isset($_POST['search'])) {
                $query = "SELECT * FROM `product` WHERE `naam` LIKE '%{$_POST['searchValue']}%'";
                $stmt = $db->prepare($query);
                $stmt->execute();
                $searchRows = $stmt->fetchAll();
                ?>
                <div class="search-container">
                    <h1>Zoeken</h1>
                    <h3>U zocht op: <?= $_POST['searchValue'] ?></h3>
            <?php

               if ($searchRows) {
                   foreach ($searchRows as $product) {
                   ?>
                       <div class="category-product">
                           <a class="product-link" href="?page=product&product=<?= $product['idproduct'] ?>">
                               <div class="product-image"
                                    style="background-image: url('<?= '/images/'.$product['afbeelding'] ?>')"
                               ></div>
                               <div class="product-info">
                                   <div class="product-name">
                                       <?= $product['naam'] ?>
                                   </div>
                                   <div class="product-price">
                                       Prijs: <?= $product['prijs'] ?> euro
                                   </div>
                                   <div class="product-rating">
                                       Beoordeling: <?= $product['gemiddelde-beoordeling'] ?>
                                   </div>
                               </div>
                           </a>
                       </div>
                    <?php
                   }
               } else {
                   echo '<div>Er zijn geen zoekresultaten gevonden.</div>';
               }
               ?>
                </div>
            <?php

            } else {
                if (isset($_GET['page'])) {
                    include('./pages/' . htmlspecialchars($_GET["page"]) . '.php');
                } else {
                    include('./pages/home.php');
                }
            }
            ?>
        </div>

        <div class="Footer"></div>
    </div>
</body>
</html>
