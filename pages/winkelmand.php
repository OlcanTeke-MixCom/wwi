<?php

if ($_POST['empty-cart']) {
    $_SESSION['cart'] = [];

    header("Refresh:0");
}
?>

<div class="cart-container">
<?php

if (count($_SESSION['cart']) != 0) {
    ?>
    <table>
    <?php
    foreach ($_SESSION['cart'] as $prodId => $cartItem) {
        ?>

        <tr>
            <td><?= $cartItem['quantity'] ?>x</td>
            <td><?= $cartItem['name'] ?></td>
        </tr>

        <?php
    }
?>
    </table>

    <form method="post" action="">
        <input class="button"
               type="submit"
               name="empty-cart"
               value="Winkelmand legen" />
    </form>

<?php
} else {

    ?>
    <div>Uw winkelmand is leeg</div>
    <?php
}
?>

</div>
