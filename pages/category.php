<?php

$Catnaam = $_GET['category'];
$sql = "Select idcategorie, naam, beschrijving, afbeelding from categorie where naam ='$Catnaam'";
$stmt = $db->prepare($sql);
$stmt->execute();
$catrow = $stmt->fetch();
$i = 0;
unset($prodrow);
unset($stmt);
$prodrow= "";
if(isset($catrow)) {
    foreach ($catrow as $columnName => $columnValue) {
        // hier in de if statements halen we eerst alle values op van te voren.
        if ($columnName == 'idcategorie') {
            $idCat = $columnValue;
        }
        if ($columnName == 'afbeelding') {
            $image = $columnValue;
            $imagePath = "/images/" . $image;
        }
        if($columnName == 'naam'){
            $name = $columnValue;
        }
        if($columnName == 'beschrijving'){
            $description = $columnValue;
        }
    }

    ?>
    <!-- hier bouwen we de HTML voor de opgehaalde waardes. -->
    <div class="category-header">
        <div class="category-image" style="background-image: url('<?= $imagePath ?>')">
            <div class='category-name'>
                <?= $name ?> <!--shorthand voor echo-->
            </div>
        </div>
    </div>

    <div class="category-content">
        <div class='category-description'>
            <?= $description ?>
        </div>

        <?php
        $prodrow = null;
        $sql = "Select * from product where idcategorie =$idCat";
        $stmt = $db->prepare($sql);
        $stmt->execute();

        //Hier bouwen we een products array met alle producten uit de categorie.
        $products = [];
        while($prodRow = $stmt->fetch()) {
            if(empty($prodRow)){
                echo "in de categorie ". $Catnaam ." zijn geen producten aanwezig.";
            } else {
                $products[] = $prodRow;
            }
        }
        ?>
        <div class="category-products">
            <?php foreach($products as $product) { ?>
                    <div class="category-product">
                        <a class="product-link" href="?page=product&product=<?= $product['idproduct'] ?>">
                            <div class="product-image"
                                 style="background-image: url('<?= '/images/'.$product['afbeelding'] ?>')"
                            ></div>
                            <div class="product-info">
                                <div class="product-name">
                                    <?= $product['naam'] ?>
                                </div>
                                <div class="product-price">
                                    Prijs: <?= $product['prijs'] ?> euro
                                </div>
                                <div class="product-rating">
                                    Beoordeling: <?= $product['gemiddelde-beoordeling'] ?>
                                </div>
                            </div>
                        </a>
                    </div>
            <?php } ?>
        </div>
    </div>
    <?php

}
