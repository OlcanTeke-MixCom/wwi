<body>
<center>
    <h1>Wachtwoord vergeten</h1>
    <form method="post" action="">
        <table>
            <tr>
                <td>E-mail:</td>
                <td>
                    <input type="text" placeholder="E-mail" name="email">
                </td>
            </tr>
            <tr>
                <td>
                    <input type="submit" name="submit" placeholder="Verzend">
                </td>
            </tr>
        </table>
    </form>

<?php

include_once 'database/DatabaseConnect.php';
$connect = new DatabaseConnect();
$db = $connect->connect();

if(isset($_POST['email'])) {

    $email = htmlspecialchars($_POST["email"]);

    $stmt = $db->prepare("SELECT email, wachtwoord FROM gebruiker WHERE email = ?;");
    $stmt->execute([$email]);
    $user = $stmt->fetch();

    if ($user) {
        $to = $user['email'];
        $subject = 'Wachtwoord aanvragen';
        $txt = 'uw aangevraagde wachtwoord'.$user['wachtwoord'];
        $headers = "From: wwi@gmail.com";
        mail($to,$subject,$txt,$headers);
    } else {
        echo 'Geen gebruiker gevonden';
    }
}
?>
</center>
