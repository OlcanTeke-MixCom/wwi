<?php

include_once '../database/DatabaseConnect.php';
$connect = new DatabaseConnect();
$db = $connect->connect();
$errors = [];
$update = null;

$prodId = $_GET['product'];
$sql = "Select * from product where idproduct = 1";
$stmt = $db->prepare($sql);
$stmt->execute();
$prodRow = $stmt->fetch();
$i = 0;
unset($stmt);
if(isset($prodRow)) {
    $product = $prodRow;
}

if(isset($_POST['add-to-cart'])) {
    $_SESSION['cart'][$prodId] = [ 'quantity' => 1, 'name' => $product['naam'] ];

    header("Refresh:0"); //dirty trick to refresh page again for the right values in cart.
}

?>

<div class="category-header">
    <div class="category-image" style="background-image: url('<?= '/images/'.$product['afbeelding'] ?>')">
        <div class='category-name'>
            <?= $product['naam'] ?> <!--shorthand voor echo-->
        </div>
    </div>
</div>
<div class="product-details-container">
    <div class="product-details-left">
        <div class="product-details-image"
             style="background-image: url('<?= '/images/'.$product['afbeelding'] ?>')"
        ></div>
    </div>
    <div class="product-details-right">
        <div class="product-details-name">
            <?= $product['naam'] ?>
        </div>
        <div class="product-details-price">
            <?= $product['prijs'] ?> euro
        </div>

        <form method="post" action="">
            <input class="button button-product-details"
                   type="submit"
                   name="add-to-cart"
                   value="In winkelmand" />
        </form>
    </div>

    <div class="product-video">
        <video width="340" height="200" controls>
            <source src="videos/<?= $product['video'] ?>" type="video/mp4">
            No video support.
        </video>
    </div>

    <?php

    $sql = "SELECT * FROM recensie WHERE idproduct = $prodId";
    $stmt = $db->prepare($sql);
    $stmt->execute();

    $reviewsRows = $stmt->fetchAll();
    unset($stmt);

    $sqlAvg = "SELECT AVG(beoordeling) FROM recensie WHERE idproduct = $prodId";
    $stmt = $db->prepare($sqlAvg);
    $stmt->execute();

    $average = $stmt->fetch();

    $updated = false;
    if (isset($_POST["write_review"])) {
        //TO-DO: en ben je ingelogd? check, en userID meesturen naar db.
        $beschrijving = htmlspecialchars($_POST['review']);
        $beoordeling = htmlspecialchars($_POST['rating']);
        $title = "Recensie van ".$_SESSION['username'];

        $sql = "INSERT INTO recensie (titel, beschtijving, beoordeling, idklant, idproduct) VALUES (?,?,?,?,?)";
        $stmt = $db->prepare($sql);
        $stmt->execute([$title, $beschrijving, $beoordeling,  $_SESSION["id"], $prodId]);
        $updated = true;
    }
    ?>

    <div class="reviews-container">
        <div class="reviews reviews-left">
        <h2>Beoordelingen:</h2>
        <h4>Gemiddelde beoordeling: <span class="score"><?= round($average['AVG(beoordeling)'], 1  ) ?></span></h4>
        <?php

        foreach ($reviewsRows as $review) {
            ?>
            <div class="review">
                <table>
                    <th><?= $review['beoordeling'] ?></th>
                    <tr><td><?= $review['titel'] ?></td></tr>
                    <tr><td><?= $review['beschtijving'] ?></td></tr>
                </table>
            </div>
            <?php
        }

        ?>
        </div>
        <div class="reviews reviews-right">
            <h2>Schrijf een beoordelingen:</h2>
            <?php
            if($_SESSION['username']) {
                echo $updated ? "<div class='green'>Uw recensie is toegevoegd.</div>" : "";
            ?>
                <form method="POST" action="">
                    <textarea name="review" cols=40 rows=3 placeholder="Uw beschrijving"></textarea>
                    <br />
                <select id="rating" name="rating">
                    <option value="1">1 ster</option>
                    <option value="2">2 ster</option>
                    <option value="3">3 ster</option>
                    <option value="4">4 ster</option>
                    <option value="5">5 ster</option>
                 </select> <br />
                    <input type="submit" value="verstuur" name="write_review" />
                </form>
            <?php } else { ?>
                <h4>U moet ingelogd zijn om een beoordeling te plaatsen.</h4>
            <?php } ?>
        </div>
    </div>
</div>
