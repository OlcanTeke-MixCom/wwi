<?php

include_once 'database/DatabaseConnect.php';
$connect = new DatabaseConnect();
$db = $connect->connect();

if (isset($_POST['submit'])){
    $email = htmlspecialchars($_POST["email"]);
    $password = htmlspecialchars($_POST["password"]);

    $stmt = $db->prepare("SELECT idklant, email, wachtwoord, gebruikersnaam FROM gebruiker WHERE email = ?;");
    $stmt->execute([$email]);
    $user = $stmt->fetch();

    if ($user && password_verify(hash('sha512', $password), $user['wachtwoord']) == $password) {
        $_SESSION["id"] = $user['idklant'];
        $_SESSION["username"] = $user['gebruikersnaam'];
        header("Location: ./");
    } else {
        echo '<p>De combinatie van uw Email en wachtwoord is incorrect</p>';
    }
}

if (isset($_GET['logout'])) {
    unset($_SESSION["id"], $_SESSION["username"]);
    header("Location: ./");
}
?>
<center>
<h1>login</h1>
<form method="post" action="./?page=login">
    <table>
        <tr>
            <td>Email adres:</td>
            <td>
                <input type="email"  placeholder="Email" name="email">
            </td>
        </tr>
        <tr>
            <td>Wachtwoord:</td>
            <td>
                <input type="Password" placeholder="Wachtwoord" name="password">
            </td>
        </tr>
        <tr>
            <td>
                <input type="submit" name="submit" placeholder="Verzend">
            </td>
        </tr>
    </table>
</form>
<a href="./?page=passwordReset">Wachtwoord vergeten?</a>
</center>
