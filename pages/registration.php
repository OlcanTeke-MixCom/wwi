<?php

include_once 'database/DatabaseConnect.php';
$connect = new DatabaseConnect();
$db = $connect->connect();
$errors = [];
$update = null;

if (isset($_POST['submit'])){
    $firstName = htmlspecialchars($_POST["naam"]);
    $lastName = htmlspecialchars($_POST['achternaam']);
    $street = htmlspecialchars($_POST['staatnaam']);
    $houseNumber = htmlspecialchars($_POST['huisnummer']);
    $addition = htmlspecialchars($_POST['toevoeging']);
    $zip = htmlspecialchars($_POST['postcode']);
    $residence = htmlspecialchars($_POST['woonplaats']);
    $customerNumber = mt_rand(100000000000000000, 999999999999999999);
    $userName = htmlspecialchars($_POST['gebruikersnaam']);
    $email = htmlspecialchars($_POST['email']);
    $password = htmlspecialchars($_POST["password"]);
    $passwordRepeat = htmlspecialchars($_POST["repeat-password"]);

    $errors['naw'] = checkUserCredentials($firstName, $lastName, $street, $houseNumber, $zip, $residence ,$userName);
    $errors['email'] = checkEmail($email, $db);
    $errors['password'] = checkPassword($password, $passwordRepeat);

    foreach ($errors as $error) {
        if (is_bool($error) !== true) {
            $update = false;
        }
    }
    if ($update !== false || $update === null) {
        $hash = password_hash(hash('sha512', $password), PASSWORD_DEFAULT);
        $sql = "INSERT INTO gebruiker (naam, achternaam, staatnaam, huisnummer, toevoeging, postcode, woonplaats, `klant-nummer`, gebruikersnaam, email, wachtwoord, idrol) VALUES (?,?,?,?,?,?,?,?,?,?,?,?)";
        $db->prepare($sql)->execute([$firstName, $lastName, $street, $houseNumber, $addition, $zip, $residence, $customerNumber, $userName, $email, $hash, 1]);
        $update = true;
    }
}

/**
 * @param $firstName
 * @param $lastName
 * @param $street
 * @param $houseNumber
 * @param $zip
 * @param $residence
 * @param $userName
 * @return bool|string
 */
function checkUserCredentials($firstName, $lastName, $street, $houseNumber, $zip, $residence ,$userName) {
    if (!$firstName || !$lastName || !$street || !$houseNumber || !$zip || !$residence || !$userName) {
        return 'Vul uw N.A.W gegevens in.';
    }
    return false;
}

/**
 * @param $email
 * @param $db
 * @return bool|string
 */
function checkEmail($email, $db) {
    if (!$email) {
        return 'Vul een e-mailadress in e-mailadres in.';
    } if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
        return 'Uw opgegeven e-mailadres is incorrect.';
    }

    $stmt = $db->prepare("SELECT email FROM gebruiker WHERE email = ?;");
    $stmt->execute([$email]);
    $user = $stmt->fetch();

    if ($user) {
        return 'Uw opgegeven e-mailadres is al geregistreerd in ons systeem.';
    }
    return false;
}

/**
 * @param $password
 * @param $passwordRepeat
 * @return bool|string
 */
function checkPassword($password, $passwordRepeat) {
    if (!$password || !$passwordRepeat || $password !== $passwordRepeat) {
        return 'Uw opgegeven wachtwoord komt niet overeen.';
    } if (strlen($password) <= 8) {
        return 'Uw opgegeven wachtwoord is te kort.';
    }
    return false;
}
?>
<center>
<h1>registration</h1>
<form method="post" action="./?page=registration">
    <table>
        <tr>
            <td>Naam:</td>
            <td>
                <input type="text" placeholder="Naam" name="naam">
            </td>
        </tr>
        <tr>
            <td>Achternaam:</td>
            <td>
                <input type="text" placeholder="Achternaam" name="achternaam">
            </td>
        </tr>
        <tr>
            <td>Staatnaam:</td>
            <td>
                <input type="text" placeholder="Staatnaam" name="staatnaam">
            </td>
        </tr>
        <tr>
            <td>Huisnummer:</td>
            <td>
                <input type="number" placeholder="Huisnummer" name="huisnummer">
            </td>
        </tr>
        <tr>
            <td>Toevoeging:</td>
            <td>
                <input type="text" placeholder="Toevoeging" name="toevoeging">
            </td>
        </tr>
        <tr>
            <td>Postcode:</td>
            <td>
                <input type="text" placeholder="Postcode" name="postcode">
            </td>
        </tr>
        <tr>
            <td>Woonplaats:</td>
            <td>
                <input type="text" placeholder="Woonplaats" name="woonplaats">
            </td>
        </tr>
        <tr>
            <td>Gebruikersnaam:</td>
            <td>
                <input type="text" placeholder="Gebruikersnaam" name="gebruikersnaam">
            </td>
        </tr>
        <tr>
            <td>Email-adres:</td>
            <td>
                <input type="email"  placeholder="Email-adres" name="email">
            </td>
        </tr>
        <tr>
            <td>Wachtwoord:</td>
            <td>
                <input type="Password" placeholder="Wachtwoord" name="password">
            </td>
        </tr>
        <tr>
            <td>Herhaal wachtwoord:</td>
            <td>
                <input type="Password" placeholder="Herhaal wachtwoord" name="repeat-password">
            </td>
        </tr>
        <tr>
            <td>
                <input type="submit" name="submit" placeholder="Verzend">
            </td>
        </tr>
    </table>
</form>
<?php
if ($errors) {
    foreach ($errors as $error) {
        echo '<p>'.$error.'</p>';
    }
} if ($update === true) {
    echo "<p>Registratie voltooid</p>";
}
?>
</center>
